from labdev-nomad.container

     cd /nomad/nomadlab/servers/nomad-vis-test/analytics/remotevis

Update info on services of labdev that we use as we share the session db (we should probably clean up this ugly command)

     kubectl exec -ti $(kubectl get po | grep nomad-container-manager-beaker | cut -f1 -d ' ') node app.js serviceDumper -- --out-file labdev-nomad.services.yaml

update config with current info on the redis dbs of labdev (default-remotevis.hjson.in -> default-remotevis.hjson)

     docker run -ti -v $PWD:/usr/src/app -v /nomad/nomadlab/servers/labdev-nomad/analytics/beaker:/mnt -w /usr/src/app --rm node:carbon node app.js templateEvaluer --replacements /mnt/labdev-nomad.services.yaml --template config/nomad-vis-test.hjson.in --out-file config/nomad-vis-test.hjson

deploy
     ./deploy.sh --tls --env nomad-vis-test --target-hostname nomad-vis-test --secret-web-certs web-certs

and execute the deploy for remote vis

     kubectl create -f container-manager-service-remotevis.yaml
     if ! kubectl get deployment nomad-container-manager-remotevis >& /dev/null ;  then
       kubectl create --save-config -f container-manager-deploy-remotevis.yaml
     else
       kubectl apply -f container-manager-deploy-remotevis.yaml
     fi

if only that changed, otherwise on has also to create the secrets and analytics namespace.

A serviceDump has to be run to reexport the ports to the frontend, then the frontend setup needs to be updated.

# labdev

docker run -ti -v $PWD:/frontend -v /nomad/nomadlab/servers/labdev-nomad/analytics/beaker:/usr/src/app -w /usr/src/app -v /nomad/nomadlab/servers/services-info:/services-info --rm node:carbon bash -c 'npm install; node app.js templateEvaluer --replacements /services-info/labdev-nomad.services.yaml --replacements /services-info/nomad-vis-test.services.yaml --replacements /frontend/config/labdev-nomad.yaml --template /frontend/nginx.conf.in --out-file /frontend/nginx-labdev-nomad.conf'

if [ ! -e /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/run ] ; then
  mkdir -p /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/run
  chown 101:101 /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/run
fi
if [ ! -e /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/cache ] ; then
  mkdir -p /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/cache
  chown 101:101 /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/cache
fi
if [ ! -e /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/client_temp ] ; then
  mkdir -p /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/client_temp
  chown 101:101 /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/client_temp
fi
docker run -d   --restart=unless-stopped   -v /root/certs:/certs:ro   -v /nomad/nomadlab/servers/labdev-nomad/web-certs:/web-certs:ro   -v /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/nginx-labdev-nomad.conf:/etc/nginx/nginx.conf:ro   -v /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/www-root:/usr/share/nginx/html:ro   -v /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/client_temp:/etc/nginx/client_temp  -v /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/cache:/var/cache/nginx -v /nomad/nomadlab/servers/labdev-nomad/frontend-nginx/run:/var/run -p 80:80 -p 443:443 -p 5509:5509 --name frontendNginx nginx

# labtest

docker run -ti -v $PWD:/frontend -v /nomad/nomadlab/servers/labdev-nomad/analytics/beaker:/usr/src/app -w /usr/src/app -v /nomad/nomadlab/servers/services-info:/services-info --rm node:carbon bash -c 'npm install; node app.js templateEvaluer --replacements /services-info/labtest-nomad.services.yaml --replacements /frontend/config/labtest-nomad.yaml --template /frontend/nginx.conf.in --out-file /frontend/nginx-labtest-nomad.conf'

docker run -d   --restart=unless-stopped   -v /root/certs:/certs:ro   -v /nomad/nomadlab/servers/labtest-nomad/web-certs:/web-certs:ro   -v /nomad/nomadlab/servers/labtest-nomad/frontend-nginx/nginx-labtest-nomad.conf:/etc/nginx/nginx.conf:ro   -v /nomad/nomadlab/servers/labtest-nomad/frontend-nginx/www-root:/usr/share/nginx/html:ro   -v /nomad/nomadlab/servers/labtest-nomad/frontend-nginx/client_temp:/etc/nginx/client_temp  -v /nomad/nomadlab/servers/labtest-nomad/frontend-nginx/cache:/var/cache/nginx -v /nomad/nomadlab/servers/labtest-nomad/frontend-nginx/run:/var/run -p 80:80 -p 443:443 -p 5509:5509 --name frontendNginx nginx
